---
title: Initial Post
date: 2016-11-29 13:36:06
tags: experience, resume
---

This is just a blog to showcase some projects I've worked and a
place for me to work out my thoughts as I make my way through a degree
in software development. May also contain some other random thoughts I
felt I needed to write down. Before I started my software development
degree I was an accountant for 6 years and during that time I discovered
the need for software skill because I was dealing with large amounts of
data and Excel just wasn't cutting it. Skills I have acquired include
Python and Javascript programming experience and database interaction,
both through database clients and software libraries. 

I built two separate Flask-backed, AngularJS-driven websites that drove
and increased the efficiency of internal reporting processes at my
previous job.

I am also very interested in financial markets and trading and have started
a small hedge fund where I manage money for myself and a few family members.
